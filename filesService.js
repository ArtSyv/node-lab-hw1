const express = require("express");
const fs = require("fs");
const path = require("path");
const morgan = require("morgan");
const app = express();

const dirname = "./files";
const supportExt = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];

app.use(express.json());
// app.use(morgan("tiny"));

function createFile(req, res) {
  if (req.body.filename === undefined || req.body.content === undefined) {
    res
      .status(400)
      .json({ message: "Please specify 'content' parameter", status: 400 });
    return;
  } else if (!supportExt.includes(path.extname(req.body.filename))) {
    res.status(400).json({ message: "Please specify extension", status: 400 });
    return;
  } else {
    fs.writeFile(
      `${dirname}/${req.body.filename}`,
      `${req.body.content}`,
      function(err) {
        if (err) throw err;
        else {
          res
            .status(200)
            .json({ message: "File created successfully", status: 200 });
        }
        return;
      }
    );
  }
}

function getFiles(req, res) {
  fs.readdir(`./files`, (err, result) => {
    if (err) {
      res.status(400).json({ message: "Client error", status: 400 });
    } else {
      res.status(200).json({ message: "Success", files: result, status: 200 });
    }
    return;
  });
}

function getFile(req, res) {
  const filename = path.basename(req.url);
  const filepath = `${dirname}/${filename}`;

  if (fs.existsSync(filepath)) {
    fs.stat(filepath, (err, date) => {
      dateFile = date.birthtime;
    });
    fs.readFile(filepath, "utf-8", (err, result) => {
      if (err) {
        throw err;
      } else {
        res.status(200).json({
          message: "Success",
          filename: filename,
          content: result,
          extension: path.extname(filename).slice(1),
          uploadedDate: dateFile
        });
      }
    });
  } else {
    res
      .status(400)
      .json({ message: `No file with '${filename}' filename found` });
  }
  return;
}

module.exports = {
  createFile,
  getFiles,
  getFile
};
